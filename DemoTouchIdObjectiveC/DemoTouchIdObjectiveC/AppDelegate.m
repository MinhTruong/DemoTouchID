//
//  AppDelegate.m
//  DemoTouchIdObjectiveC
//
//  Created by TruongVO07 on 5/31/17.
//  Copyright © 2017 TruongVO. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  ViewController *vc = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
  [self.window setRootViewController:vc];
  [self.window makeKeyAndVisible];
  return YES;
}

@end
