//
//  AppDelegate.h
//  DemoTouchIdObjectiveC
//
//  Created by TruongVO07 on 5/31/17.
//  Copyright © 2017 TruongVO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

