//
//  ViewController.m
//  DemoTouchIdObjectiveC
//
//  Created by TruongVO07 on 5/31/17.
//  Copyright © 2017 TruongVO. All rights reserved.
//

#import "ViewController.h"
#import <LocalAuthentication/LocalAuthentication.h>

@interface ViewController ()
{
  __weak IBOutlet UILabel *_statusLabel;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)onLoginClick:(id)sender {
  LAContext *context = [[LAContext alloc] init];
  NSError *error;
  if (![context canEvaluatePolicy:LAPolicyDeviceOwnerAuthentication error:&error]) {
    NSString *ms = @"This device does not have a TouchID sensor.";
    _statusLabel.text = ms;
  } else {
    [context evaluatePolicy:LAPolicyDeviceOwnerAuthentication localizedReason:@"Only awesome people are allowed" reply:^(BOOL success, NSError * _Nullable error) {
      if (success) {
        [NSOperationQueue.mainQueue addOperationWithBlock:^{
          _statusLabel.text = @"Success";
        }];
      } else if (error != nil) {
        NSString *ms = [self errorMessageForLAErrorCode:error.code];
        [NSOperationQueue.mainQueue addOperationWithBlock:^{
          _statusLabel.text = ms;
        }];
      }
    }];
  }
}

- (NSString *) errorMessageForLAErrorCode:(NSInteger)code {
  NSString *message = @"";
  switch (code) {
    case LAErrorAppCancel:
      message = @"Authentication was cancelled by application";
      break;
    case LAErrorAuthenticationFailed:
      message = @"The user failed to provide valid credentials";
      break;
    case LAErrorInvalidContext:
      message = @"The context is invalid";
      break;
    case LAErrorPasscodeNotSet:
      message = @"Passcode is not set on the device";
      break;
    case LAErrorSystemCancel:
      message = @"Authentication was cancelled by the system";
      break;
    case LAErrorTouchIDLockout:
      message = @"Too many failed attempts.";
      break;
    case LAErrorTouchIDNotAvailable:
      message = @"TouchID is not available on the device";
      break;
    case LAErrorUserCancel:
      message = @"The user did cancel";
      break;
    case LAErrorUserFallback:
      message = @"The user chose to use the fallback";
      break;
      default:
      message = @"Did not find error code on LAError object";
  }
  return message;
}

@end
