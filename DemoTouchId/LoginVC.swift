//
//  LoginVC.swift
//  DemoTouchId
//
//  Created by TruongVO07 on 5/31/17.
//  Copyright © 2017 TruongVO. All rights reserved.
//

import UIKit
import LocalAuthentication

class LoginVC: UIViewController {
  @IBOutlet weak var loginButton: UIButton!
  @IBOutlet weak var statusLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    loginButton.addTarget(self, action: #selector(onLogin), for: .touchUpInside)
  }
  
  func onLogin() {
    let context = LAContext()
    var err: NSError?
    
    guard context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &err) else {
      let ms = "This device does not have a TouchID sensor."
      statusLabel.text = ms
      print(ms)
      return
    }
    
    context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: "Only awesome people are allowed") { (success, error) in
      if success {
        let ms = "Success"
        DispatchQueue.main.async(execute: { 
          self.statusLabel.text = ms
          print(ms)
        })
      } else {
        if let error = error as NSError? {
          let message = self.errorMessageForLAErrorCode(errorCode: error.code)
          print(message)
          DispatchQueue.main.async(execute: {
            self.statusLabel.text = message
          })
        }
      }
    }
  }
  
  func errorMessageForLAErrorCode( errorCode:Int ) -> String{
    var message = ""
    
    switch errorCode {
      
    case LAError.appCancel.rawValue:
      message = "Authentication was cancelled by application"
      
    case LAError.authenticationFailed.rawValue:
      message = "The user failed to provide valid credentials"
      
    case LAError.invalidContext.rawValue:
      message = "The context is invalid"
      
    case LAError.passcodeNotSet.rawValue:
      message = "Passcode is not set on the device"
      
    case LAError.systemCancel.rawValue:
      message = "Authentication was cancelled by the system"
      
    case LAError.touchIDLockout.rawValue:
      message = "Too many failed attempts."
      
    case LAError.touchIDNotAvailable.rawValue:
      message = "TouchID is not available on the device"
      
    case LAError.userCancel.rawValue:
      message = "The user did cancel"
      
    case LAError.userFallback.rawValue:
      message = "The user chose to use the fallback"
      
    default:
      message = "Did not find error code on LAError object"
      
    }
    return message
    
  }
}
