//
//  AppDelegate.swift
//  DemoTouchId
//
//  Created by TruongVO07 on 5/31/17.
//  Copyright © 2017 TruongVO. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    window = UIWindow(frame: UIScreen.main.bounds)
    let vc = LoginVC(nibName: "LoginVC", bundle: nil)
    window!.rootViewController = vc
    window?.makeKeyAndVisible()
    return true
  }
}

